<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('authlevel:yonetici')->prefix('admin')->group(function () {

    Route::get('/', 'AdminController@index');

    Route::get('/siniflar', 'AdminController@allClasses');
    Route::get('/sinif/update/{sinifID}', 'AdminController@updateClass');
    Route::post('/sinif/update', 'AdminController@updateClass');
    Route::get('/sinif/ekle', 'AdminController@addClass');
    Route::post('/sinif/ekle', 'AdminController@addClass');

    Route::get('/ogretmenler', 'AdminController@allTeachers');
    Route::get('/ogretmen/update/{ogretmenID}', 'AdminController@updateTeacher');
    Route::post('/ogretmen/update', 'AdminController@updateTeacher');
    Route::get('/ogretmen/ekle', 'AdminController@addTeacher');
    Route::post('/ogretmen/ekle', 'AdminController@addTeacher');

    Route::get('/dersler', 'AdminController@allLessons');
    Route::get('/ders/update/{dersID}', 'AdminController@updateLesson');
    Route::post('/ders/update/{dersID}', 'AdminController@updateLesson');
    Route::get('/ders/ekle', 'AdminController@addLesson');
    Route::post('/ders/ekle', 'AdminController@addLesson');

    Route::get('/ogrenciler', 'AdminController@allStudents');
    Route::get('/ogrenciler/search', 'AdminController@searchStudent');
    Route::get('/ogrenci/update/{userID}', 'AdminController@updateStudents');
    Route::post('/ogrenci/update/{userID}', 'AdminController@updateStudents');
    Route::get('/ogrenci/ekle', 'AdminController@addStudents');
    Route::post('/ogrenci/ekle', 'AdminController@addStudents');

    Route::get('/duyurular', 'AdminController@allAnnouncements');
    Route::get('/duyurular/search', 'AdminController@searchAnnouncements');
    Route::get('/duyuru/update/{duyuruID}', 'AdminController@updateAnnouncements');
    Route::post('/duyuru/update/{duyuruID}', 'AdminController@updateAnnouncements');
    Route::get('/duyuru/ekle', 'AdminController@addAnnouncements');
    Route::post('/duyuru/ekle', 'AdminController@addAnnouncements');
    Route::get('/duyuru/sil/{duyuruID}', 'AdminController@deleteAnnouncements');

    Route::get('/yoklama/al', 'AdminController@bugunolanderdler');
    Route::get('/yoklama/yazdir/{id}', 'AdminController@yoklamaYazdir');

});
