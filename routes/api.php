<?php

use Illuminate\Http\Request;

/* CORS Ayarları */
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('login', 'Auth\ApiAuthController@login');

Route::group(['middleware' => ['auth:api']], function () { // Token ile Yetkilendirilmiş Kullanıcılar

    Route::post('logout', 'Auth\ApiAuthController@logout');

    Route::get('duyurular/sinif', 'AnnouncementController@sinifDuyurulari');
    Route::get('duyurular/genel', 'AnnouncementController@genelDuyurular');

    Route::get('dersprogrami/student', 'StudentController@ogreciDersProgrami')->middleware('authlevel:ogrenci');
    Route::post('qrcode/onay', 'StudentController@qrCodeOnay')->middleware('authlevel:ogrenci');

    Route::get('dersprogrami/teacher', 'TeacherController@ogretmenDersProgrami')->middleware('authlevel:ogretmen');

});
