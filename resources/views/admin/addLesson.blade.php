@extends('layouts.admin')

@section('title', 'Yeni Ders Ekle')

@section('content')
    <div class="col-12">
        <form action="/admin/ders/ekle" method="post">
            <div class="form-group col">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </div>
            <div class="form-group col">
                <label for="name">Ders Adı</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="MATEMATİK" autocomplete="off" required>
            </div>
            <div class="form-group col">
                <label for="class_id">Dersin Sınıfı</label>
                <select id="class_id" name="class_id" class="form-control">
                    @foreach($siniflar as $sinif)
                        <option value="{{$sinif->id}}">{{$sinif->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col">
                <label for="teacher">Derse Girecek Öğretmeni Seçiniz</label>
                <select id="teacher" name="teacher" class="form-control">
                    @foreach($ogretmenler as $ogretmen)
                        <option value="{{$ogretmen->user_id}}">{{$ogretmen->name}} {{$ogretmen->surname}}</option>
                    @endforeach
                </select>
            </div>

            @php
                $saatler = [
                    '08:30-09:20', '09:30-10:20',
                    '10:30-11:20', '11:30-12:20',
                    '12:30-13:20', '13:30-14:20',
                    '14:30-15:20', '15:30-16:20',
                    '16:30-17:20', '17:30-18:20',
                ];

                $etiketler = [
                    '08:30:00', '09:30:00',
                    '10:30:00', '11:30:00',
                    '12:30:00', '13:30:00',
                    '14:30:00', '15:30:00',
                    '16:30:00', '17:30:00',
                ];
            @endphp

            <div class="form-group col-3 float-left">
                <div class="card">
                    <div class="card-title">
                        Pazartesi
                    </div>
                    <div class="card-body">
                        @for($i = 0; $i < 10; $i++)
                            <div class="form-check">
                                <input class="form-check-input" name="pazartesi[]" type="checkbox" value="{{$etiketler[$i]}}" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    {{ $saatler[$i] }}
                                </label>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>

            <div class="form-group col-3 float-left">
                <div class="card">
                    <div class="card-title">
                        Salı
                    </div>
                    <div class="card-body">
                        @for($i = 0; $i < 10; $i++)
                            <div class="form-check">
                                <input class="form-check-input" name="sali[]" type="checkbox" value="{{$etiketler[$i]}}" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    {{ $saatler[$i] }}
                                </label>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>

            <div class="form-group col-3 float-left">
                <div class="card">
                    <div class="card-title">
                        Çarşamba
                    </div>
                    <div class="card-body">
                        @for($i = 0; $i < 10; $i++)
                            <div class="form-check">
                                <input class="form-check-input" name="carsamba[]" type="checkbox" value="{{$etiketler[$i]}}" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    {{ $saatler[$i] }}
                                </label>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>

            <div class="form-group col-3 float-left">
                <div class="card">
                    <div class="card-title">
                        Perşembe
                    </div>
                    <div class="card-body">
                        @for($i = 0; $i < 10; $i++)
                            <div class="form-check">
                                <input class="form-check-input" name="persembe[]" type="checkbox" value="{{$etiketler[$i]}}" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    {{ $saatler[$i] }}
                                </label>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>

            <div class="form-group col-3 float-left">
                <div class="card">
                    <div class="card-title">
                        Cuma
                    </div>
                    <div class="card-body">
                        @for($i = 0; $i < 10; $i++)
                            <div class="form-check">
                                <input class="form-check-input" name="cuma[]" type="checkbox" value="{{$etiketler[$i]}}" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    {{ $saatler[$i] }}
                                </label>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>

            <div class="form-group col-3 float-left">
                <div class="card">
                    <div class="card-title">
                        Cumartesi
                    </div>
                    <div class="card-body">
                        @for($i = 0; $i < 10; $i++)
                            <div class="form-check">
                                <input class="form-check-input" name="cumartesi[]" type="checkbox" value="{{$etiketler[$i]}}" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    {{ $saatler[$i] }}
                                </label>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>

            <div class="form-group col-3 float-left">
                <div class="card">
                    <div class="card-title">
                        Pazar
                    </div>
                    <div class="card-body">
                        @for($i = 0; $i < 10; $i++)
                            <div class="form-check">
                                <input class="form-check-input" name="pazar[]" type="checkbox" value="{{$etiketler[$i]}}" id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    {{ $saatler[$i] }}
                                </label>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>

            <div class="form-group col-3 float-left">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group col-12">
                            <button class="btn-lg btn-dark" type="submit">Dersi Ekle</button>
                        </div>
                    </div>
                <div>
            </div>
        </form>
    </div>
@endsection
