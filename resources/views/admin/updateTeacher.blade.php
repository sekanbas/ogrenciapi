@extends('layouts.admin')

@section('title', 'Öğretmen Bilgilerini Güncelle')

@section('content')
    <div class="col-12">
        <form action="/admin/ogretmen/update" method="post">
            <input type="hidden" name="user_id" value="{{$ogretmen->user_id}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-inline">
                <label for="name">Öğretmen Adı</label>
                <input type="text" class="form-control" id="name" name="name" value="{{$ogretmen->name}}">
            </div>
            <div class="form-inline">
                <label for="surname">Öğretmen Soyadı</label>
                <input type="text" class="form-control" id="surname" name="surname" value="{{$ogretmen->surname}}">
            </div>
            <div class="form-inline">
                <button type="submit" class="btn btn-dark">Bilgileri Güncelle</button>
            </div>
        </form>
    </div>
@endsection

