@extends('layouts.admin')

@section('title', 'Duyurular')

@section('content')
    <div class="row">
        <div class="col-6 float-left">
            <form action="/admin/duyurular/search" method="get" class="form-horizontal">
                <div class="row form-group">
                    <div class="col col-md-12">
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button class="btn btn-dark">
                                    <i class="fa fa-search"></i> Duyuru Başlığı
                                </button>
                            </div>
                            <input type="text" id="search_title" name="search_title" placeholder="3.Veli Toplantısı" class="form-control" required>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-6 float-right">
            <a href="/admin/duyuru/ekle" class="float-right">
                <button class="btn btn-dark">
                    <i class="fa fa-user-plus"></i> Yeni Duyuru Ekle
                </button>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-12 pt-3">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Duyuru Başlığı</th>
                    <th scope="col">Hedef</th>
                    <th scope="col">Yayınlanma Tarihi</th>
                    <th scope="col">Güncellenme Tarihi</th>
                    <th scope="col">İşlemler</th>
                </tr>
                </thead>
                <tbody>
                @foreach($duyurular as $duyuru)
                    <tr>
                        <th scope="row">{{$duyuru->title}}</th>
                        <td>{{$duyuru->getClass()->name}}</td>
                        <td>{{$duyuru->created_at}}</td>
                        <td>{{$duyuru->updated_at}}</td>
                        <td>
                            <a href="/admin/duyuru/update/{{$duyuru->id}}">
                                <button class="btn btn-dark">Bilgileri Görüntüle</button>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
        <div class="col-12 float-right">
            {{ $duyurular->links() }}
        </div>
    </div>
@endsection
