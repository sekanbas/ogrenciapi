<html>
    <head>
        <title></title>

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css">
        <style type="text/css">
            @media print
            {
                .page-break  { display:block; page-break-before:always; }

            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-10">

                </div>
            </div>
            <div class="row mt-5">
                <div class="col-10">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Öğrenci Numarası</th>
                            <th scope="col">Adı Soyadı</th>
                            <th scope="col">QR Code</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($records as $record)
                            @php
                                $ogrenci = \App\Student::find($record->student_id);
                            @endphp

                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$ogrenci->student_number}}</td>
                                <td>{{$ogrenci->name}} {{$ogrenci->surname}}</td>
                                <td name="barcodetd">{{$record->barcode}}
                                    <p id="barcode" style="display: none">
                                        {{$record->barcode}}
                                    </p>
                                    <div id="qrcode{{$loop->iteration}}">
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <script src="https://cdn.jsdelivr.net/npm/jquery@2.2.4/dist/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="/assets/qr/jquery.qrcode.js"></script>
        <script type="text/javascript" src="/assets/qr/qrcode.js"></script>

        <script type="text/javascript">
            $("td[name='barcodetd']").each(function(index) {
                $(this).find('div').qrcode({
                    text: $(this).find('div#barcode').text(),
                    width: 150,
                    height: 150
                });
            });
        </script>

    </body>
</html>
