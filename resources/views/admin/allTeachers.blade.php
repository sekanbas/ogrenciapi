@extends('layouts.admin')

@section('title', 'Öğretmenler')

@section('content')
    <div class="card">
        <div class="col-12 float-right mb-3">
            <a href="/admin/ogretmen/ekle">
                <button class="btn btn-dark">
                    <i class="fa fa-plus"></i> Yeni Öğretmen Ekle
                </button>
            </a>
        </div>
        <div class="col-12">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Adı</th>
                    <th scope="col">Soyadı</th>
                    <th scope="col">E-Mail</th>
                    <th scope="col">İşlemler</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($ogretmenler as $ogretmen)
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{ $ogretmen->name }}</td>
                        <td>{{ $ogretmen->surname }}</td>
                        <td>{{ $ogretmen->getUser->email }}</td>
                        <td>
                            <a href="/admin/ogretmen/update/{{$ogretmen->user_id}}">
                                <button type="button" class="btn btn-dark">Detaylar</button>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
