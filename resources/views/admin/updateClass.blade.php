@extends('layouts.admin')

@section('title', 'Sınıf Bilgilerini Güncelle')

@section('content')
    <div class="col-12">
        <form action="/admin/sinif/update" method="post">
            <input type="hidden" name="id" value="{{$sinif->id}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-inline">
                <label for="name">Sınıf Adı</label>
                <input type="text" class="form-control ml-2 mr-2" id="name" name="name" placeholder="12-MF" value="{{$sinif->name}}">
                <button type="submit" class="btn btn-dark">Bilgileri Güncelle</button>
            </div>
        </form>
    </div>

    <div class="row">
        <div class="col-12 pt-5">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Öğrenci NO</th>
                    <th scope="col">Adı</th>
                    <th scope="col">Soyadı</th>
                    <th scope="col">Detay</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $ogrenciler = $sinif->getStudents()->get();
                @endphp
                @foreach($ogrenciler as $ogrenci)
                    <tr>
                        <th scope="row">{{$ogrenci->student_number}}</th>
                        <td>{{$ogrenci->name}}</td>
                        <td>{{$ogrenci->surname}}</td>
                        <td>
                            <a href="/admin/ogrenci/update/{{$ogrenci->user_id}}">
                                <button class="btn btn-dark">Bilgileri Görüntüle</button>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
