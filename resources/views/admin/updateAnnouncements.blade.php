@extends('layouts.admin')

@section('title', 'Duyuru İçeiriğini Güncelle')

@section('content')
    <div class="container">
        <form action="/admin/duyuru/update/{{$duyuru->id}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group ">
                <label for="title">Duyuru Başlığı</label>
                <input type="text" class="form-control" id="title" name="title" value="{{$duyuru->title}}" required autocomplete="off">
            </div>
            <div class="form-group">
                <label for="content">Duyuru İçeriği</label>
                <textarea id="content" name="content" class="form-control" rows="10">{{$duyuru->content}}</textarea>
            </div>
            <div class="form-group mt-auto col-6 float-left">
                <button type="submit" class="btn btn-dark">Güncelle</button>
            </div>
        </form>
        <div class="form-group mt-auto col-6 float-left">
            <a href="/admin/duyuru/sil/{{$duyuru->id}}"><button class="btn btn-danger">Sil</button></a>
        </div>
    </div>
@endsection
