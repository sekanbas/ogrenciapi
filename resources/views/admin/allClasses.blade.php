@extends('layouts.admin')

@section('title', 'Sınıflar')

@section('content')
    <div class="card">
        <div class="col-12 pb-3">
            <a href="/admin/sinif/ekle" class="float-right">
                <button class="btn btn-dark">
                    <i class="fa fa-plus"></i> Yeni Sınıf Ekle
                </button>
            </a>
        </div>
        <div class="col-12">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Sınıf Adı</th>
                    <th scope="col">Mevcut</th>
                    <th scope="col">İşlemler</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($siniflar as $sinif)
                    <tr>
                        <th scope="row">{{$loop->iteration}}</th>
                        <td>{{ $sinif->name }}</td>
                        <td>{{ $sinif->getStudents()->count() }}</td>
                        <td>
                            <a href="/admin/sinif/update/{{$sinif->id}}">
                                <button type="button" class="btn btn-dark">Detaylar</button>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
