@extends('layouts.admin')

@section('title', 'Öğrenci Bilgilerini Güncelle')

@section('content')
    <div class="container">
        <form action="/admin/ogrenci/update/{{$user->id}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group ">
                <label for="name">Öğrenci Adı </label>
                <input type="text" class="form-control" id="name" name="name" value="{{$ogrenci->name}}" required autocomplete="off">
            </div>
            <div class="form-group">
                <label for="surname">Öğrenci Soyadı </label>
                <input type="text" class="form-control" id="surname" name="surname" value="{{$ogrenci->surname}}" required autocomplete="off">
            </div>
            <div class="form-group">
                <label for="student_number">Öğrenci Numarası </label>
                <input type="number" class="form-control" id="student_number" name="student_number" value="{{$ogrenci->student_number}}" required autocomplete="off">
            </div>
            <div class="form-group">
                <label for="email">Öğrenci E-Mail Adresi </label>
                <input type="email" class="form-control" id="email" name="email" value="{{$user->email}}" required autocomplete="off">
            </div>
            <div class="form-group">
                <label for="class_id">Öğrenciinin Sınıfı</label>
                <select id="class_id" name="class_id" class="form-control">
                    @foreach($siniflar as $sinif)
                        <option value="{{$sinif->id}}" {{($ogrenci->class_id == $sinif->id)?'selected':''}}>{{$sinif->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="password">Öğrenci Şifresi </label>
                <input type="password" class="form-control" id="password" name="password">
            </div>
            <div class="form-group ">
                <button type="submit" class="btn btn-success">Güncelle</button>
            </div>
        </form>
    </div>
@endsection
