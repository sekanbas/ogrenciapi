@extends('layouts.admin')

@section('title', 'Dersler')

@section('content')
        <div class="col-12 float-right mb-4">
            <a href="/admin/ders/ekle">
                <button class="btn btn-dark">
                    <i class="fa fa-plus"></i> Yeni Ders Ekle
                </button>
            </a>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Adı</th>
                <th scope="col">Sınıf</th>
                <th scope="col">Öğretmen</th>
                <th scope="col">İşlemler</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($dersler as $ders)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{ $ders->name }}</td>
                    <td>{{ $ders->getClass->name }}</td>
                    <td>{{ $ders->getTeacher->name }} {{ $ders->getTeacher->surname }}</td>
                    <td>
                        <a href="/admin/ders/update/{{$ders->id}}">
                            <button type="button" class="btn btn-dark">Detaylar</button>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
@endsection
