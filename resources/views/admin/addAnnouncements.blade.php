@extends('layouts.admin')

@section('title', 'Duyuru Yap')

@section('content')
    <div class="container">
        <form action="/admin/duyuru/ekle" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group ">
                <label for="title">Duyuru Başlığı</label>
                <input type="text" class="form-control" id="title" name="title" value="" required autocomplete="off">
            </div>
            <div class="form-group">
                <label for="content">Duyuru İçeriği</label>
                <textarea id="content" name="content" class="form-control" rows="10"></textarea>
            </div>
            <div class="form-group mt-4">
                <label for="all">Tüm Sınıflar İçin Duyuru Yap</label>
                <input type="checkbox" name="all" />
            </div>
            <div class="row">
                <div class="form-group mt-1 col-6 float-left">
                    <label for="classes[]">Duyuru Alacak Sınıflar </label>
                    <select id="classes[]" name="classes[]" data-placeholder="Sınıfları seçiniz ..." multiple class="standardSelect">
                        @foreach($siniflar as $sinif)
                            <option value="{{$sinif->id}}" >{{$sinif->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group mt-auto col-6 float-left">
                    <button type="submit" class="btn btn-dark">Duyuru Yap</button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('script')
    <script src="/assets/js/lib/chosen/chosen.jquery.min.js"></script>
    <script>
        jQuery(document).ready(function() {
            jQuery(".standardSelect").chosen({
                disable_search_threshold: 10,
                no_results_text: "Böyle dir sınıf bulanamadı!",
                width: "100%"
            });
        });
    </script>
@endsection
