@extends('layouts.admin')

@section('title', 'Yeni Öğretmen Ekle')

@section('content')
    <div class="col-12">
        <form action="/admin/ogretmen/ekle" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-inline col-6">
                <label for="name" class="col-form-label col-5">Öğretmen Adı</label>
                <div class="col-7">
                    <input type="text" required class="form-control" id="name" name="name">
                </div>
            </div>

            <div class="form-inline col-6">
                <label for="surname" class="col-5 col-form-label">Öğretmen Soyadı</label>
                <div class="col-7">
                    <input type="text" required class="form-control" id="surname" name="surname">
                </div>
            </div>

            <div class="form-inline col-6">
                <label for="email" class="col-5 col-form-label">Öğretmen E-Mail Adresi</label>
                <div class="col-7">
                    <input type="email" required class="form-control" id="email" name="email">
                </div>
            </div>

            <div class="form-inline col-6">
                <label for="password" class="col-5 col-form-label">Hesap Şifresi</label>
                <div class="col-7">
                    <input type="password" required class="form-control" id="password" name="password">
                </div>
            </div>

            <div class="form-group col-6">
                <button type="submit" class="btn btn-dark">Ekle</button>
            </div>
        </form>
    </div>
@endsection
