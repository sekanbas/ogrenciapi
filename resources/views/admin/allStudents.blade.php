@extends('layouts.admin')

@section('title', 'Öğrenci Listeleme')

@section('content')
    <div class="row">
        <div class="col-12 pb-3">
            <a href="/admin/ogrenci/ekle" class="float-right">
                <button class="btn btn-dark">
                    <i class="fa fa-user-plus"></i> Yeni Öğrenci Ekle
                </button>
            </a>
        </div>
        <div class="col-6">
            <form action="/admin/ogrenciler/search" method="get" class="form-horizontal">
                <div class="row form-group">
                    <div class="col col-md-12">
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button class="btn btn-dark">
                                    <i class="fa fa-search"></i> Öğrenci Adı
                                </button>
                            </div>
                            <input type="text" id="search_name" name="search_name" placeholder="Ali" class="form-control" required>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-6">
            <form action="/admin/ogrenciler/search" method="get" class="form-horizontal">
                <div class="row form-group">
                    <div class="col col-md-12">
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button class="btn btn-dark">
                                    <i class="fa fa-search"></i> Öğrenci NO
                                </button>
                            </div>
                            <input type="text" id="search_number" name="search_number" placeholder="141816000" class="form-control" required>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-12 pt-3">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Öğrenci NO</th>
                    <th scope="col">Adı</th>
                    <th scope="col">Soyadı</th>
                    <th scope="col">Detay</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($ogrenciler as $ogrenci)
                        <tr>
                            <th scope="row">{{$ogrenci->student_number}}</th>
                            <td>{{$ogrenci->name}}</td>
                            <td>{{$ogrenci->surname}}</td>
                            <td>
                                <a href="/admin/ogrenci/update/{{$ogrenci->user_id}}">
                                    <button class="btn btn-dark">Bilgileri Görüntüle</button>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>
        <div class="col-12 float-right">
            {{ $ogrenciler->links() }}
        </div>
    </div>
@endsection
