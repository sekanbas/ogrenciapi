@extends('layouts.admin')

@section('title', 'Yeni Sınıf Ekle')

@section('content')
    <div class="col-12">
        <form action="/admin/sinif/ekle" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-inline">
                <label for="name">Sınıf Adı </label>
                <input type="text" class="form-control ml-2 mr-2" id="name" name="name" placeholder="12-MF" value="">
                <button type="submit" class="btn btn-dark">Ekle</button>
            </div>
        </form>
    </div>
@endsection
