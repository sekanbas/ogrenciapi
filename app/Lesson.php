<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed id
 * @property mixed class_id
 * @property mixed teacher
 */
class Lesson extends Model
{
    protected $fillable = ['name', 'class_id', 'teacher'];

    public function getClass(){
        return $this->hasOne('App\Sinif', 'id', 'class_id');
    }

    public function getTeacher(){
        return $this->hasOne('App\Teacher', 'user_id', 'teacher');
    }

    public function getCalendar(){
        return $this->hasMany('App\LessonCalendar', 'lesson_id', 'id');
    }

}
