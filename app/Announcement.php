<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

/**
 * Class Announcement
 * @package App
 * @property mixed id
 * @property mixed class_id
 * @property mixed title
 * @property mixed content
 */
class Announcement extends Model
{
    protected $fillable = ['class_id', 'title', 'content'];

    public function getClass(){
        if($this->class_id == 0)
        {
            return new \App\Sinif([
                'name' => 'Genel Duyuru'
            ]);

        }else{
            return \App\Sinif::find($this->class_id);
        }
    }
}
