<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class AuthLevel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next, $role)
    {
        if(Auth::check()){
            switch ($role) {
                case 'ogrenci':
                    if(Auth::user()->auth_level === 0){
                        return $next($request);
                    }
                    break;
                case 'veli':
                    if(Auth::user()->auth_level === 1){
                        return $next($request);
                    }
                    break;
                case 'ogretmen':
                    if(Auth::user()->auth_level === 2){
                        return $next($request);
                    }
                    break;
                case 'yonetici':
                    if(Auth::user()->auth_level === 3){
                        return $next($request);
                    }
                    break;
                default:
                    abort(401);
            }
        }

        abort(401);

    }
}
