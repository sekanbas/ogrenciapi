<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class TeacherController extends Controller
{

    public function ogretmenDersProgrami(){
        $user = Auth::user();
        $ogretmen = \App\Teacher::find($user->id);
        $dersler = $ogretmen->getLessons()->get();
        $tmpDersler = [];

        foreach ($dersler as $ders) {
            array_push($tmpDersler, $ders->id);
        }

        $cevapArray = [];

        for($i = 0; $i < 7; $i++){
            array_push($cevapArray,
                \App\LessonCalendar::join('lessons', 'lesson_calendars.lesson_id', '=', 'lessons.id')
                    ->join('classes', 'lessons.class_id', '=', 'classes.id')
                    ->select('lesson_calendars.*', 'classes.name as class_name', 'lessons.*')
                    ->whereIn('lesson_id', $tmpDersler)
                    ->where('day', $i)
                    ->orderBy('clock', 'asc')->get()
            );
        }

        return $cevapArray;
    }
}
