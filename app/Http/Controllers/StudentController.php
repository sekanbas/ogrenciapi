<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    public function ogreciDersProgrami(){
        $user = Auth::user();
        $ogrenci = \App\Student::find($user->id);
        $sinif = \App\Sinif::find($ogrenci->class_id);
        $dersler = $sinif->getLessons()->get();
        $tmpDersler = [];

        foreach ($dersler as $ders) {
            array_push($tmpDersler, $ders->id);
        }

        $cevapArray = [];

        for($i = 0; $i < 7; $i++){
            array_push($cevapArray,
               \App\LessonCalendar::join('lessons', 'lesson_calendars.lesson_id', '=', 'lessons.id')->whereIn('lesson_id', $tmpDersler)->where('day', $i)->orderBy('clock', 'asc')->get()
            );
        }

        return $cevapArray;
    }

    public function qrCodeOnay(Request $request){
        $barkod = ($request->has('barcode')?$request->get('barcode'):null);
        $record = \App\Inspection::where('student_id', '=', Auth::user()->id)->where('barcode', '=', $barkod);

        if($record){
            $record->delete();
            response()->json([
                'msg' => 'Yoklama İşlemi Başarılı bir şekilde tamamlandı.',
                'code' => 200,
            ], 200);
        }else{
            response()->json([
                'msg' => 'Bilgileriniz yanlış.',
                'code' => 401,
            ], 401);
        }
    }
}
