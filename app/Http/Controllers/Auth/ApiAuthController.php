<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiAuthController extends Controller
{
    public function login(Request $request){

        if (auth()->attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            $user = auth()->user();

            if($user->auth_level > 2) // Yönetici veya Admin API'ye Erişemez
            {
                return response()->json([
                    'msg' => 'Lütfen girmiş olduğunuz bilgilerin eksiksiz ve doğru olduğundan emin olun !',
                    'code' => 401,
                ], 401);
            }


            if($request->has('device_id')){
                if(blank($user->device_id)){ // Kullanıcı ilk defa giriş yapıyor ise
                    $user->device_id = $request->get('device_id');
                }else{
                    if($user->device_id != $request->get('device_id')){ // Farklı Cihazdan Giriş Yapmaya Çalışıyor ise
                        $user->api_token = null;
                        $user->save();

                        return response()->json([
                            'msg' => 'Tanımsız Kullanıcı Cihazı',
                            'code' => 401,
                        ], 401);
                    }
                }
            }else{
                return response()->json([
                    'msg' => 'Bad Request',
                    'code' => 400,
                ], 400);
            }

            $user->api_token = str_random(60);
            $user->save();
            return $user;
        }

        return response()->json([
            'msg' => 'Lütfen girmiş olduğunuz bilgilerin eksiksiz ve doğru olduğundan emin olun !',
            'code' => 401,
        ], 401);
    }

    public function logout(Request $request){
        if (auth()->user()) {
            $user = auth()->user();
            $user->api_token = null;
            $user->save();

            return response()->json([
                'message' => 'Başarılı bir şekilde çıkış yapıldı',
            ], 200);
        }

        return response()->json([
            'error' => 'Çıkış yapılamıyor',
            'code' => 401,
        ], 401);
    }
}
