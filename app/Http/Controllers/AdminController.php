<?php

namespace App\Http\Controllers;

use App\Http\Middleware\Authenticate;
use App\Inspection;
use Illuminate\Http\Request;
use Illuminate\Notifications\AnonymousNotifiable;
use Illuminate\View\View;
use Illuminate\Support\Facades\Hash;

use App\Sinif;
use App\Teacher;
use App\User;
use App\Lesson;
use App\LessonCalendar;
use App\Message;
use App\Student;
use App\Announcement;

class AdminController extends Controller
{
    public function index(){
        return View('admin.index');
    }

    public function allClasses(){
        $siniflar = Sinif::all();
        return View('admin.allClasses')->with('siniflar', $siniflar);
    }

    public function addClass(Request $request){
        if($request->isMethod('get')){
            return View('admin.addClass');
        }else{
            $sinif = new Sinif();
            $sinif->name = $request->get('name');
            $sinif->save();

            return redirect()->to('admin/siniflar');
        }
    }

    public function updateClass(Request $request, $sinifID = 0){
        if($request->isMethod('get')){
            $sinif = Sinif::find($sinifID);
            return View('admin.updateClass')->with('sinif', $sinif);
        }else{
            $sinif = Sinif::find($request->id);
            $sinif->name = $request->get('name');
            $sinif->save();

            return redirect()->back();
        }
    }

    public function allTeachers(){
        $ogretmenler = Teacher::all();
        return View('admin.allTeachers')->with('ogretmenler', $ogretmenler);
    }

    public function addTeacher(Request $request){
        if($request->isMethod('get')){
            return View('admin.addTeacher');
        }else{
            $user = new User();
            $user->name = $request->name.' '.$request->surname;
            $user->email = $request->email;
            $user->auth_level = 2;
            $user->password = Hash::make($request->password);
            $user->save();

            $ogretmen = new Teacher();
            $ogretmen->user_id = $user->id;
            $ogretmen->name = $request->get('name');
            $ogretmen->surname = $request->get('surname');
            $ogretmen->save();

            return redirect()->to('admin/ogretmenler');
        }
    }

    public function updateTeacher(Request $request, $ogretmenID = 0){
        if($request->isMethod('get')){
            $ogretmen = Teacher::find($ogretmenID);
            return View('admin.updateTeacher')->with('ogretmen', $ogretmen);
        }else{
            $ogretmen = Teacher::find($request->user_id);
            $ogretmen->name = $request->get('name');
            $ogretmen->surname = $request->get('surname');
            $ogretmen->save();

            return redirect()->back();
        }
    }

    public function allLessons(){
        $dersler = Lesson::all();
        return View('admin.allLessons')->with('dersler', $dersler);
    }

    public function addLesson(Request $request){
        if($request->isMethod('get')){
            $siniflar = Sinif::all();
            $ogretmenler = Teacher::all();

            return View('admin.addLesson')->with('siniflar', $siniflar)->with('ogretmenler', $ogretmenler);
        }else{

            if($request->hasAny('pazartesi', 'sali', 'carsamba', 'persembe', 'cuma', 'cumartesi', 'pazar')) {

                // En az bir ders saati seçili olmalı

                $ders = new Lesson();
                $ders->name = $request->name;
                $ders->class_id = $request->class_id;
                $ders->teacher = $request->teacher;
                $ders->save();

                $gunler = [
                    $request->has('pazar')?$request->pazar:[],
                    $request->has('pazartesi')?$request->pazartesi:[],
                    $request->has('sali')?$request->sali:[],
                    $request->has('carsamba')?$request->carsamba:[],
                    $request->has('persembe')?$request->persembe:[],
                    $request->has('cuma')?$request->cuma:[],
                    $request->has('cumartesi')?$request->cumartesi:[]
                ];

                for($j = 0; $j < 7; $j++){
                    $gun = $gunler[$j];
                    for($i = 0; $i < count($gun); $i++){
                        $takvim = new LessonCalendar();
                        $takvim->lesson_id = $ders->id;
                        $takvim->day = $j;
                        $takvim->clock = $gun[$i];
                        $takvim->save();
                    }
                }

                return redirect()->back()->with('msg', Message::success('Ders başarılı bir şeklide kayıt edildi.'));

            }else{
                return redirect()->back()->with('msg', Message::error('Lütfen en bir ders saati olacak şekilde ders ekleyiniz.'));
            }

        }

    }

    public function updateLesson(Request $request, $lessonID){
        if($request->isMethod('get')){
            $siniflar = Sinif::all();
            $ogretmenler = Teacher::all();
            $ders = Lesson::find($lessonID);
            return View('admin.updateLesson')
                ->with('siniflar', $siniflar)
                ->with('ogretmenler', $ogretmenler)
                ->with('ders', $ders);
        }else{

            if($request->hasAny('pazartesi', 'sali', 'carsamba', 'persembe', 'cuma', 'cumartesi', 'pazar')) {

                // En az bir ders saati seçili olmalı

                $ders = Lesson::find($lessonID);
                $oldrecords = $ders->getCalendar;

                foreach ($oldrecords as $tmp) {
                    $tmp->delete();
                }

                $ders->name = $request->name;
                $ders->class_id = $request->class_id;
                $ders->teacher = $request->teacher;
                $ders->save();

                $gunler = [
                    $request->has('pazar') ? $request->pazar : [],
                    $request->has('pazartesi') ? $request->pazartesi : [],
                    $request->has('sali') ? $request->sali : [],
                    $request->has('carsamba') ? $request->carsamba : [],
                    $request->has('persembe') ? $request->persembe : [],
                    $request->has('cuma') ? $request->cuma : [],
                    $request->has('cumartesi') ? $request->cumartesi : []
                ];

                for ($j = 0; $j < 7; $j++) {
                    $gun = $gunler[$j];
                    for ($i = 0; $i < count($gun); $i++) {
                        $takvim = new LessonCalendar();
                        $takvim->lesson_id = $ders->id;
                        $takvim->day = $j;
                        $takvim->clock = $gun[$i];
                        $takvim->save();
                    }
                }

                return redirect()->back()->with('msg', Message::success('Ders başarılı bir şeklide güncellendi.'));

            }else{
                return redirect()->back()->with('msg', Message::error('Lütfen en bir ders saati olacak şekilde dersi güncelleyiniz.'));
            }
        }
    }

    public function allStudents(){
        $ogrenciler = Student::paginate(25);
        return View('admin.allStudents')->with('ogrenciler', $ogrenciler);
    }

    public function searchStudent(Request $request){

        $ogrenciler = null;

        if($request->has('search_name')){
            $ogrenciler = Student::where('name', 'LIKE', '%'.$request->get('search_name').'%')->paginate(25);
        }else{
            $ogrenciler = Student::where('student_number', '=', $request->get('search_number'))->paginate(25);
        }

        return View('admin.allStudents')->with('ogrenciler', $ogrenciler);
    }

    public function addStudents(Request $request){
        if($request->isMethod('get')){
            $siniflar = Sinif::all();
            return View('admin.addStudent')->with('siniflar', $siniflar);
        }else{

            $user = new User();
            $user->name = $request->get('name').' '.$request->get('surname');
            $user->email = $request->get('email');
            $user->password = Hash::make($request->get('password'));
            $user->save();

            $ogrenci = new Student();
            $ogrenci->user_id = $user->id;
            $ogrenci->name = $request->get('name');
            $ogrenci->surname = $request->get('surname');
            $ogrenci->class_id = $request->get('class_id');
            $ogrenci->student_number = $request->get('student_number');
            $ogrenci->save();

            return redirect()->to('admin/ogrenci/update/'.$user->id);
        }
    }


    public function updateStudents(Request $request, $userID){
        if($request->isMethod('get')){
            $ogrenci = Student::find($userID);
            $user = $ogrenci->getUser;
            $siniflar = Sinif::all();
            return View('admin.updateStudent')
                ->with('siniflar', $siniflar)
                ->with('ogrenci', $ogrenci)
                ->with('user', $user);
        }else{
            $ogrenci = Student::find($userID);
            $user = User::find($userID);

            $user->name = $request->get('name').' '.$request->get('surname');
            $user->email = $request->get('email');

            if($request->get('password') != null && $request->get('password') != '')
                $user->password = Hash::make($request->get('password'));

            $user->save();

            $ogrenci->name = $request->get('name');
            $ogrenci->surname = $request->get('surname');
            $ogrenci->class_id = $request->get('class_id');
            $ogrenci->student_number = $request->get('student_number');
            $ogrenci->save();

            return redirect()->back();
        }
    }


    public function allAnnouncements(){
        $duyurular = Announcement::orderBy('title', 'desc')->paginate(25);
        return View('admin.allAnnouncements')->with('duyurular', $duyurular);
    }

    public function searchAnnouncements(Request $request){

        if($request->has('search_title')){
            $duyurular = Announcement::where('title', 'LIKE', '%'.$request->get('search_title').'%')->orderBy('title', 'desc')->paginate(25);
            return View('admin.allAnnouncements')->with('duyurular', $duyurular);
        }else{
            return $this->allAnnouncements();
        }

    }

    public function addAnnouncements(Request $request){
        if($request->isMethod('get')){
            $siniflar = Sinif::all();
            return View('admin.addAnnouncements')->with('siniflar', $siniflar);
        }else{

            if($request->has('title', 'content')){

                if($request->has('all')){
                    $duyuru = new Announcement();
                    $duyuru->title = $request->get('title');
                    $duyuru->content = $request->get('content');
                    $duyuru->class_id = 0; // Genel Duyuru
                    $duyuru->save();
                }else{
                    if($request->has('classes')){
                        foreach ($request->get('classes') as $item) {
                            $duyuru = new Announcement();
                            $duyuru->title = $request->get('title');
                            $duyuru->content = $request->get('content');
                            $duyuru->class_id = $item;
                            $duyuru->save();
                        }
                    }else{
                        return redirect()->to('/admin/duyurular')
                            ->with('msg', Message::error('Duyuru ekleme işlemi başarısız oldu. Lütfen duyurunun hangi sınıflarla paylaşılacağını belirtiniz.'));
                    }
                }

                return redirect()->to('/admin/duyurular')
                    ->with('msg', Message::success('Duyuru başarılı bir şekilde sisteme eklendi.'));
            }else{
                return redirect()->to('/');
            }
        }
    }

    public function updateAnnouncements(Request $request, $duyuruID){
        if($request->isMethod('get')){
            if(Announcement::find($duyuruID)){
                $duyuru = Announcement::find($duyuruID);
                return View('admin.updateAnnouncements')->with('duyuru', $duyuru);
            }else{
                abort(400);
            }
        }else{
            if($request->has( 'title', 'content')){
                $duyuru = Announcement::find($duyuruID);
                $duyuru->title = $request->get('title');
                $duyuru->content = $request->get('content');
                $duyuru->save();

                return redirect()->to('/admin/duyurular')
                    ->with('msg', Message::success('Duyuru başarılı bir şekilde güncellendi.'));
            }else{
                return redirect()->to('/');
            }
        }
    }

    public function deleteAnnouncements(Request $request, $duyuruID){
        if(Announcement::find($duyuruID)){
            $duyuru = Announcement::find($duyuruID)->delete();
            return redirect()->to('/admin/duyurular')
                ->with('msg', Message::success('Duyuru başarılı bir şekilde silindi.'));
        }else{
            return redirect()->to('/admin/duyurular')
                ->with('msg', Message::error('Duyuru silme işlemi başarısız oldu.'));
        }
    }

    public function bugunolanderdler(){

        $gunun_dersleri = LessonCalendar::where('day', '=', date('w'))->orderBy('clock', 'desc')->get();

        return View('admin.bugununDersleri')->with('records', $gunun_dersleri);
    }

    public function yoklamaYazdir(Request $request, $id){

        $calendar = LessonCalendar::find($id);

        if($calendar){

            if(Inspection::where('lesson_calendar_id', $calendar->id)->count() > 0){
                $records = Inspection::where('lesson_calendar_id', $calendar->id)->get();
                return view('admin.yoklamaYazdir')->with('records', $records);
            }

            $lesson = $calendar->getLesson;
            $sinif = $lesson->getClass;
            $ogrenciler = $sinif->getStudents;

            foreach ($ogrenciler as $ogrenci){
                $inspect = new Inspection();
                $inspect->lesson_calendar_id = $calendar->id;
                $inspect->student_id = $ogrenci->user_id;
                $inspect->barcode = str_random(60);
                $inspect->save();
            }

            $records = Inspection::where('lesson_calendar_id', $calendar->id)->get();

            return view('admin.yoklamaYazdir')->with('records', $records);
        }else{
            abort(400);
        }
    }







}
