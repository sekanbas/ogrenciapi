<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Announcement;
use Illuminate\Support\Facades\Auth;

class AnnouncementController extends Controller
{
    public function genelDuyurular(){
        return Announcement::where('class_id', '=', 0)->orderBy('title', 'desc')->limit(25)->get();
    }

    public function sinifDuyurulari(){
        $student = \App\Student::find(Auth::user()->id);
        return Announcement::where('class_id', '=', $student->class_id)->orderBy('title', 'desc')->limit(25)->get();
    }
}
