<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HomeworkControl extends Model
{
    protected $fillable = ['homework_id', 'student_id', 'state'];
}
