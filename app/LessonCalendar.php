<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer lesson_id
 * @property string day
 * @property string clock
 */
class LessonCalendar extends Model
{
    protected $fillable = ['lesson_id', 'day', 'clock'];

    public function getLesson(){
        return $this->hasOne('App\Lesson', 'id', 'lesson_id');
    }
}
