<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Student
 * @property mixed user_id
 * @property mixed name
 * @property mixed surname
 * @property mixed student_number
 * @property mixed class_id
 * @property mixed getUser
 */
class Student extends Model
{

    protected $primaryKey = 'user_id';

    protected $fillable = ['user_id', 'name', 'surname', 'student_number', 'class_id'];

    public function getUser(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
