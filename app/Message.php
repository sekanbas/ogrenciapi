<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public static function success($content){
        return [
            'type' => 1,
            'content' => $content
        ];
    }

    public static function error($content){
        return [
            'type' => 0,
            'content' => $content
        ];
    }
}
