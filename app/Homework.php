<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Homework extends Model
{
    protected $fillable = ['lesson_id', 'title', 'content', 'end_date'];
}
