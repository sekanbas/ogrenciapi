<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sinif extends Model
{
    protected $table = 'classes';

    protected $fillable = ['name'];

    public function getStudents(){
        return $this->hasMany('App\Student', 'class_id', 'id');
    }

    public function getLessons(){
        return $this->hasMany('App\Lesson', 'class_id', 'id');
    }
}
