<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{

    protected $primaryKey = 'user_id';

    protected $fillable = ['user_id', 'name', 'surname'];

    public function getUser(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function getLessons(){
        return $this->hasMany('App\Lesson', 'teacher', 'user_id');
    }
}
