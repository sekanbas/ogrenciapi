<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Inspection
 * @package App
 * @property mixed lesson_calendar_id
 * @property mixed student_id
 * @property mixed barcode
 */
class Inspection extends Model
{

    protected $fillable = ['lesson_calendar_id', 'student_id', 'barcode'];

    public function getLessonCalendar(){
        return $this->hasOne('App\LessonCalendar', 'id', 'lesson_calendar_id');
    }
}
